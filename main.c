#include <stdio.h>

#define PI 3.14

int main() {
    float anzahlStuecke = 12;       /* Anzahl Tortenstücke */
    float hoehe = 8;                /* Hoehe der Torte in cm */
    float durchmesser = 28;         /* Durchmesser der Torte in cm */

    float radius = durchmesser / 2; /* Radius der Torte in cm */

    float gesamtvolumen = radius * radius * PI * hoehe;

    float einzelvolumen = gesamtvolumen / anzahlStuecke;

    printf("Volumen eines Tortenstuecks: %f cm3\n", einzelvolumen);
    printf("Volumen eines Tortenstuecks: %f l\n", einzelvolumen/1000);
    return 0;
}